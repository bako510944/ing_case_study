/**
 * Listens for changes in the file input field and updates the UI accordingly.
 */
document.getElementById('fileInput').addEventListener('change', function() {
    const fileInput = document.getElementById('fileInput');
    const file = fileInput.files[0];

    // Display the selected file name
    if (file) {
        const selectedFileName = document.getElementById('selectedFileName');
        selectedFileName.textContent = `Selected file: ${file.name}`;
    } else {
        const selectedFileName = document.getElementById('selectedFileName');
        selectedFileName.textContent = '';
    }
});

/**
 * Listens for clicks on the upload button and handles file upload.
 */
document.getElementById('uploadBtn').addEventListener('click', async function() {
    const fileInput = document.getElementById('fileInput');
    const file = fileInput.files[0];

    if (file) {
        try {
            await uploadFile(file);
        } catch (error) {
            console.error('Error uploading file:', error);
            displayMessage('Error uploading file. Please try again.', 'error');
            setTimeout(clearMessage, 3000);
        }
    } else {
        displayMessage('Please select a file.', 'error');
        setTimeout(function() {
            clearMessage();
        }, 10000);
    }

});
/**
 * Uploads the given file to the server.
 * @param {File} file - The file to upload.
 */
async function uploadFile(file) {
    // Check if the file is a CSV or Excel file
    const isCsv = file.type === 'text/csv';
    const isExcel = file.type === 'application/vnd.ms-excel' || file.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

    // Upload the file based on its type
    if (isCsv) {
        const formData = new FormData();
        formData.append('csvFile', file);

        try {
            const response = await fetch('http://localhost:3000/records', {
                method: 'POST',
                body: formData
            });
            await handleResponse(response);
        } catch (error) {
            handleError(error);
        }
    } else if (isExcel) {
        const formData = new FormData();
        formData.append('excelFile', file);

        try {
            const response = await fetch('http://localhost:3000/trueHits', {
                method: 'POST',
                body: formData
            });
            await handleResponse(response);
        } catch (error) {
            handleError(error);
        }
    } else {
        // If it's neither CSV nor Excel, display an error message.
        displayMessage('Unsupported file type. Please upload a CSV or Excel file.', 'error');
        setTimeout(clearMessage, 10000);
    }
}

/**
 * Handles the response from the server after file upload.
 * @param {Response} response - The response from the server.
 */
function handleResponse(response) {
    if (response.ok) {
        displayMessage('File uploaded successfully.', 'success');
        setTimeout(clearMessage, 3000);
    } else {
        console.error('Failed to upload file:', response.statusText);
        displayMessage('Failed to upload file. Please try again.', 'error');
        // Remove the message after 3 seconds
        setTimeout(clearMessage, 3000);
    }
}

/**
 * Handles errors that occur during file upload.
 * @param {Error} error - The error that occurred.
 */
function handleError(error) {
    console.error('Error uploading file:', error);
    displayMessage('Error uploading file. Please try again.', 'error');
    // Remove the message after 3 seconds
    setTimeout(clearMessage, 3000);
}
/**
 * Displays a message on the UI.
 * @param {string} message - The message to display.
 * @param {string} type - The type of message (e.g., 'success', 'error').
 */
function displayMessage(message, type) {
    const statusMessage = document.getElementById('statusMessage');
    statusMessage.textContent = message;
    statusMessage.className = type;
}
/**
 * Clears any messages displayed on the UI.
 */
function clearMessage() {
    const statusMessage = document.getElementById('statusMessage');
    statusMessage.textContent = '';
}

