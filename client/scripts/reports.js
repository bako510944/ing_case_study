/**
 * Attaches an event listener to the "Fetch Report" button to fetch and display the report.
 */
document.getElementById('fetchReportBtn').addEventListener('click', function() {
    fetchReport();
});

/**
 * Fetches the report from the server and displays it in a table format.
 */
function fetchReport() {
    fetch('http://localhost:3000/reports')
        .then(response => {
            if (response.ok) {
                return response.text();
            } else {
                throw new Error('Failed to fetch report');
            }
        })
        .then(async report => {
            console.log(report);
            displayReportInTable(report);
        })
        .catch(error => {
            console.error('Error fetching report:', error);
        });
}

/**
 * Displays the report in a table format.
 * @param {string} report - The report content to be displayed.
 */
function displayReportInTable(report) {
    const reportContent = document.getElementById('reportContent');
    const lines = report.trim().split('\n');

    // Check if the report table already exists in the DOM, if not, create it
    let table = document.getElementById('reportTable');
    if (!table) {
        table = document.createElement('table');
        table.id = 'reportTable';
        reportContent.appendChild(table);
    }

    // Check if the table already has a tbody, if not, create one
    let tbody = table.querySelector('tbody');
    if (!tbody) {
        tbody = document.createElement('tbody');
        table.appendChild(tbody);
    }

    // Create data rows with values
    for (let i = 0; i < lines.length; i++) {
        const rowData = lines[i].split(',').map(data => data.trim());
        const row = document.createElement('tr');
        rowData.forEach(data => {
            const cell = document.createElement('td');
            cell.textContent = data;
            row.appendChild(cell);
        });
        tbody.appendChild(row);
    }
}
