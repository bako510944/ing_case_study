/**
 * Attaches an event listener to the "Send Report" button to send the report to users.
 */
document.getElementById('sendReportBtn').addEventListener('click', async () => {
    await sendReportToUsers();
});

/**
 * Sends the report to all users.
 */
async function sendReportToUsers() {
    try {
        // Fetch the report and users
        const report = await getReport();
        const users = getUsers();

        // Send the report to users
        const response = await fetch('http://localhost:3000/distributions', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ users, report })
        });

        if (!response.ok) {
            throw new Error('Failed to send report to users');
        }

        alert('Report sent to all users successfully');
    } catch (error) {
        console.error('Error sending report to users:', error);
        alert('Error sending report to users. Please try again later.');
    }
}

/**
 * Retrieves the list of users from the server.
 * @returns {Array} The list of users.
 */
async function getUsers() {
    try {
        const response = await fetch('http://localhost:3000/users');
        if (!response.ok) {
            throw new Error('Failed to fetch users');
        }
        const users = await response.json();
        console.log(users);
    } catch (error) {
        console.error('Error fetching users:', error);
    }
}

/**
 * Retrieves the report from the server.
 * @returns {Object} The report object.
 */
async function getReport() {
    try {
        const response = await fetch('http://localhost:3000/reports');
        if (!response.ok) {
            throw new Error('Failed to fetch report');
        }
        const report = await response.json();
        console.log(report);
    } catch(error) {
        console.error('Error fetching the report: ', error);
    }
}
