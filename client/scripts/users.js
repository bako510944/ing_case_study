/**
 * Attaches an event listener to the "Fetch Users" button to retrieve and display the list of users.
 */
document.getElementById('fetchUsersBtn').addEventListener('click', fetchUsers);

/**
 * Attaches an event listener to the "Add User" button to prompt the user for a new email and add the user.
 */
document.getElementById('addUserBtn').addEventListener('click', async () => {
    const newUserEmail = prompt('Enter email for new user:');
    if (newUserEmail) {
        await addUser(newUserEmail);
    }
});

/**
 * Retrieves the list of users from the server and displays them in the UI.
 */
async function fetchUsers() {
    try {
        const response = await fetch('http://localhost:3000/users');
        if (!response.ok) {
            throw new Error('Failed to fetch users');
        }
        const users = await response.json();
        console.log(users);
        await displayUsers(users);
    } catch (error) {
        console.error('Error fetching users:', error);
    }
}

/**
 * Displays the list of users in the UI.
 * @param {Array} users - The list of users to display.
 */
async function displayUsers(users) {
    const userList = document.getElementById('userList');
    userList.innerHTML = ''; // Clear previous content
    users.forEach(user => {
        const row = document.createElement('tr');
        row.innerHTML = `
            <td>${user.id}</td>
            <td>${user.email}</td>
            <td>
                <button class="updateBtn" data-id="${user.id}">Update</button>
                <button class="deleteBtn" data-id="${user.id}">Delete</button>
            </td>
        `;
        userList.appendChild(row);
    });
    await attachButtonListeners();
}

/**
 * Attaches event listeners to the update and delete buttons for each user.
 */
async function attachButtonListeners() {
    const updateButtons = document.querySelectorAll('.updateBtn');
    updateButtons.forEach(button => {
        button.addEventListener('click', () => {
            const userId = button.dataset.id;
            updateUser(userId);
        });
    });

    const deleteButtons = document.querySelectorAll('.deleteBtn');
    deleteButtons.forEach(button => {
        button.addEventListener('click', () => {
            const userId = button.dataset.id;
            deleteUser(userId);
        });
    });
}

/**
 * Adds a new user with the specified email to the server.
 * @param {string} email - The email of the new user.
 */
async function addUser(email) {
    try {
        const response = await fetch('http://localhost:3000/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email })
        });
        if (!response.ok) {
            throw new Error('Failed to add user');
        }
        // Refresh user list after adding user
        await fetchUsers();
    } catch (error) {
        console.error('Error adding user:', error);
    }
}

/**
 * Updates the email of the user with the specified ID.
 * @param {string} userId - The ID of the user to update.
 */
async function updateUser(userId) {
    try {
        // Prompt the user for a new email
        const newEmail = prompt('Enter new email:');
        if (!newEmail) return; // If the user cancels or enters an empty email, return nothing.

        const response = await fetch(`http://localhost:3000/users/${userId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: newEmail })
        });

        if (!response.ok) {
            throw new Error('Failed to update user');
        }

        // Refresh user list after updating user
        await fetchUsers();
    } catch (error) {
        console.error('Error updating user:', error);
    }
}

/**
 * Deletes the user with the specified ID from the server.
 * @param {string} userId - The ID of the user to delete.
 */
async function deleteUser(userId) {
    try {
        const response = await fetch(`http://localhost:3000/users/${userId}`, {
            method: 'DELETE'
        });

        if (!response.ok) {
            throw new Error('Failed to delete user');
        }

        // Refresh user list after deleting user
        await fetchUsers();
    } catch (error) {
        console.error('Error deleting user:', error);
    }
}









