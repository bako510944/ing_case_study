export function createTrueHitTable(db) {
    return new Promise((resolve, reject) => {
        const createTableQuery = `
        CREATE TABLE IF NOT EXISTS TrueHits (
            RECORD_ID TEXT,
            SYSTEM_ID TEXT,
            BATCH_ID TEXT,
            STATE_NAME TEXT,
            Name TEXT,
            Date_Of_Birth TEXT,
            Country TEXT,
            UNIT TEXT,
            Type_of_Record TEXT,
            Hit_ID TEXT,
            Filtering_Date TEXT,
            Decision_Date TEXT,
            Alerts INTEGER,
            ORIGIN TEXT,
            DESIGNATION TEXT,
            KEYWORD TEXT,
            User_1 TEXT,
            User_2 TEXT,
            USERDATA2 TEXT
        );
        `;
        db.run(createTableQuery, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log('TrueHit table created successfully.');
                resolve();
            }
        });
    });
}


export function insertOrUpdateTrueHit(db,
                                      RECORD_ID,
                                      SYSTEM_ID,
                                      BATCH_ID,
                                      STATE_NAME,
                                      Name,
                                      Date_Of_Birth,
                                      Country,
                                      UNIT,
                                      Type_of_Record,
                                      Hit_ID,
                                      Filtering_Date,
                                      Decision_Date,
                                      Alerts,
                                      ORIGIN,
                                      DESIGNATION,
                                      KEYWORD,
                                      User_1,
                                      User_2,
                                      USERDATA2) {
    return new Promise((resolve, reject) => {
        const insertOrUpdateQuery = `
        INSERT INTO TrueHits (RECORD_ID,
                              SYSTEM_ID,
                              BATCH_ID,
                              STATE_NAME,
                              Name,
                              Date_Of_Birth,
                              Country,
                              UNIT,
                              Type_of_Record,
                              Hit_ID,
                              Filtering_Date,
                              Decision_Date,
                              Alerts,
                              ORIGIN,
                              DESIGNATION,
                              KEYWORD,
                              User_1,
                              User_2,
                              USERDATA2)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
        db.run(insertOrUpdateQuery, [RECORD_ID,
            SYSTEM_ID,
            BATCH_ID,
            STATE_NAME,
            Name,
            Date_Of_Birth,
            Country,
            UNIT,
            Type_of_Record,
            Hit_ID,
            Filtering_Date,
            Decision_Date,
            Alerts,
            ORIGIN,
            DESIGNATION,
            KEYWORD,
            User_1,
            User_2,
            USERDATA2], function (err) {
            if (err) {
                reject(err);
            } else {
                if (this.changes > 0) {
                    console.log(`Inserted or updated trueHit with RECORD_ID: ${RECORD_ID}`);
                } else {
                    console.log(`TrueHit with ID ${RECORD_ID} is up to date.`);
                }
                resolve();
            }
        });
    });
}

export async function deleteTrueHitsData(db) {
    try {
        // Delete records with 'TER / OBSOLETE' in 'BATCH_ID' column
        await deleteRecordsByBatchId(db, 'TER / OBSOLETE');

        // Delete 'PIL' records with 'ORIGIN' column equals to 'Italy'
        await deleteRecordsByOrigin(db, 'PIL', 'Italy');

        // Delete records with null or empty values in 'User_1' and 'BATCH_ID' columns
        await deleteRecordsWithNullValues(db, ['User_1', 'BATCH_ID']);

        console.log('Deletion of TrueHits data completed successfully.');
    } catch (error) {
        console.error('An error occurred during deletion of TrueHits data:', error);
    }
}

async function deleteRecordsByBatchId(db, batchId) {
    const deleteQuery = `DELETE FROM TrueHits WHERE BATCH_ID = ?`;
    await runDeleteQuery(db, deleteQuery, batchId);
}

async function deleteRecordsByOrigin(db, origin, country) {
    const deleteQuery = `DELETE FROM TrueHits WHERE ORIGIN = ? AND Country = ?`;
    await runDeleteQuery(db, deleteQuery, origin, country);
}

async function deleteRecordsWithNullValues(db, columns) {
    const deleteQuery = `DELETE FROM TrueHits WHERE ${columns.map(col => `${col} IS NULL OR ${col} = ''`).join(' OR ')}`;
    await runDeleteQuery(db, deleteQuery);
}

async function runDeleteQuery(db, query, ...params) {
    return new Promise((resolve, reject) => {
        db.run(query, params, function (err) {
            if (err) {
                reject(err);
            } else {
                console.log(`Deleted ${this.changes} records.`);
                resolve();
            }
        });
    });
}

