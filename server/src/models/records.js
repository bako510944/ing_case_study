export function createRecordTable(db) {
    return new Promise((resolve, reject) => {
        const createTableQuery = `
            CREATE TABLE IF NOT EXISTS Records (
                ID TEXT PRIMARY KEY,
                VALUE TEXT,
                PEP_STATUS TEXT,
                HISTORY TEXT
            );
        `;
        db.run(createTableQuery, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log('Record table created successfully.');
                resolve();
            }
        });
    });
}

export function insertOrUpdateRecord(db, ID, VALUE, PEP_STATUS, HISTORY) {
    return new Promise((resolve, reject) => {
        const insertOrUpdateQuery = `
            INSERT INTO Records (ID, VALUE, PEP_STATUS, HISTORY)
            VALUES (?, ?, ?, ?)
            ON CONFLICT(ID) DO UPDATE SET
            VALUE=excluded.VALUE,
            PEP_STATUS=excluded.PEP_STATUS,
            HISTORY=excluded.HISTORY;
        `;
        db.run(insertOrUpdateQuery, [ID, VALUE, PEP_STATUS, HISTORY], function(err) {
            if (err) {
                reject(err);
            } else {
                if (this.changes > 0) {
                    console.log(`Inserted or updated record with ID: ${ID}`);
                } else {
                    console.log(`Record with ID ${ID} is up to date.`);
                }
                resolve();
            }
        });
    });
}