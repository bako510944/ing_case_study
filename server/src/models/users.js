import { connectDatabase } from '../db/dbConnection.js';
let db = connectDatabase();
export function createUserTable() {
    return new Promise((resolve, reject) => {
        const createTableQuery = `
        CREATE TABLE IF NOT EXISTS Users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            email TEXT UNIQUE
        );
        `;
        db.run(createTableQuery, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log('Users table created successfully.');
                resolve();
            }
        })
    })
}

export function getAllUsers() {
    return new Promise((resolve, reject) => {
        const getAllUsersQuery = `SELECT * FROM Users`;
        db.all(getAllUsersQuery, (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}
export function getUserById(id) {
    return new Promise((resolve, reject) => {
        const selectUserQuery = 'SELECT id FROM Users WHERE id = ?';
        db.get(selectUserQuery, [id], (err, row) => {
            if (err) {
                reject(err);
            } else {
                if (row) {
                    console.log(`User with ID ${id} found:`, row);
                    resolve(row);
                } else {
                    reject(new Error(`User with ID ${id} not found`));
                }
            }
        });
    });
}
export function addUser(email) {
    return new Promise((resolve, reject) => {
        const addUserQuery = `INSERT INTO Users (Email) VALUES (?)`;
        db.run(addUserQuery, [email], function (err) {
            if (err) {
                reject(err);
            } else {
                console.log(`User added with ID: ${this.lastID}`);
                resolve(this.lastID);
            }
        });
    });
}

export function updateUser(id, email) {
    return new Promise((resolve, reject) => {
        const updateUserQuery = `UPDATE Users SET Email = ? WHERE id = ?`;
        db.run(updateUserQuery, [email, id], function (err) {
            if (err) {
                reject(err);
            } else {
                console.log(`User updated with ID: ${id}`);
                resolve();
            }
        });
    });
}

export function deleteUserById(id) {
    return new Promise((resolve, reject) => {
        const deleteUserQuery = `DELETE FROM Users WHERE id = ?`;
        db.run(deleteUserQuery, [id], function (err) {
            if (err) {
                reject(err);
            } else {
                console.log(`User deleted with ID: ${id}`);
                resolve();
            }
        });
    });
}

