import sqlite3 from 'sqlite3';

export function connectDatabase(callback) {
    const db = new sqlite3.Database('src/dbING.db', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE);
    
    db.on('error', (err) => {
        console.error('Error connecting to the database:', err);
    });

    db.on('open', () => {
        console.log('Connected to the database.');
        if (callback) {
            callback(db);
        }
    });

    return db;
}

export function closeDatabaseConnection(db) {
    db.close((err) => {
        if (err) {
            console.error('Error closing database connection:', err);
        } else {
            console.log('Database connection closed successfully.');
        }
    });
}
