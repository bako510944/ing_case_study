import { connectDatabase, closeDatabaseConnection } from './dbConnection.js';
import { ensureDatabaseConnection,
    getDataFromCSVFiles,
    createDatabaseTables,
    insertDataIntoTables,
    getDataFromExcelFiles,
    deleteDataAndFiles } from './dbTasks.js'
import { generateReportAndWriteToFile } from "../utils/generateReport.js";

export async function dbExecution() {
    let db;
    try {
        db = connectDatabase();
        await ensureDatabaseConnection(db);
        const dataFromCSVFiles = await getDataFromCSVFiles();
        const dataFromExcelFile = await getDataFromExcelFiles();
        await createDatabaseTables(db);
        await insertDataIntoTables(db, dataFromCSVFiles, dataFromExcelFile);
        await deleteDataAndFiles(db);
        await generateReportAndWriteToFile(db);

    } catch (error) {
        console.error('An error occurred:', error);
    } finally {
        if (db) {
            closeDatabaseConnection(db);
        }
    }
}

