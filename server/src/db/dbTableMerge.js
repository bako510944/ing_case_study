export async function mergeRecordsWithTrueHits(db) {
    try {
        const mergeQuery = `
            SELECT
                r.ID,
                r.VALUE,
                th.UNIT,
                th.Hit_ID
            FROM
                Records r
            INNER JOIN
                TrueHits th ON r.ID = th.User_1;
        `;

        return new Promise((resolve, reject) => {
            db.all(mergeQuery, (err, rows) => {
                if (err) {
                    console.error('An error occurred while merging records with TrueHits:', err);
                    reject(err);
                } else {
                    // console.log('Merged records with TrueHits:', rows);
                    console.log('Merged records with TrueHits Successfully.');
                    resolve(rows);
                }
            });
        });
    } catch (error) {
        console.error('An error occurred:', error);
        throw error;
    }
}
