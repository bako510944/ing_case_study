import { createRecordTable, insertOrUpdateRecord } from '../models/records.js';
import {createTrueHitTable, insertOrUpdateTrueHit, deleteTrueHitsData} from "../models/trueHits.js";
import { parseCSVFiles } from '../utils/csv/csvParser.js';
import {deleteCsvFiles, getCsvFiles} from '../utils/csv/csvFileRead.js';
import { calculateHistory } from '../utils/historyCalculator.js';
import {getExcelFilePath, expectedAttributes, deleteExcelFile} from "../utils/excel/excelFileRead.js";
import { parseExcel } from "../utils/excel/excelParser.js";
import { createUserTable } from "../models/users.js";

export async function ensureDatabaseConnection(db) {
    return db;
}

export async function getDataFromCSVFiles() {
    const csvFiles = await getCsvFiles();
    return parseCSVFiles(csvFiles);
}
export async function getDataFromExcelFiles() {
    const filePath = await getExcelFilePath();
    if (!filePath) {
        console.log('No Excel file found. Exiting.');
        return;
    }
    return parseExcel(filePath,expectedAttributes, 1);
}

export  async function createDatabaseTables(db) {
    await createRecordTable(db);
    await createTrueHitTable(db);
    await createUserTable(db);
}
export async function insertDataIntoTables(db, recordData, trueHitsData) {
    for (const record of recordData) {
        const history = calculateHistory(record.PEP_STATUS);
        await insertOrUpdateRecord(db, record.ID, record.VALUE, record.PEP_STATUS, history);
    }

    if (trueHitsData && Symbol.iterator in Object(trueHitsData)) {
        for (const trueHit of trueHitsData) {
            const {
                RECORD_ID,
                SYSTEM_ID,
                BATCH_ID,
                STATE_NAME,
                Name,
                Date_Of_Birth,
                Country,
                UNIT,
                Type_of_Record,
                Hit_ID,
                Filtering_Date,
                Decision_Date,
                Alerts,
                ORIGIN,
                DESIGNATION,
                KEYWORD,
                User_1,
                User_2,
                USERDATA2
            } = trueHit;

            // Insert or update the trueHit record using individual field values
            await insertOrUpdateTrueHit(db,
                RECORD_ID,
                SYSTEM_ID,
                BATCH_ID,
                STATE_NAME,
                Name,
                Date_Of_Birth,
                Country,
                UNIT,
                Type_of_Record,
                Hit_ID,
                Filtering_Date,
                Decision_Date,
                Alerts,
                ORIGIN,
                DESIGNATION,
                KEYWORD,
                User_1,
                User_2,
                USERDATA2);
        }
    } else {
        console.log('trueHitsData is not iterable or is null/undefined.');
    }
}

export async function deleteDataAndFiles(db) {
    await deleteCsvFiles();
    await deleteExcelFile();
    await deleteTrueHitsData(db);
}


