import multer from 'multer';
/**
 * Gets original file names when uploading file
 */
const storageCSV = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '../server/src/files/Initialization files/');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
const storageExcel = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '../server/src/files/Input files/');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

export const getExcelFilename = multer({ storage: storageExcel });
export const getCSVFilename = multer({ storage: storageCSV });
