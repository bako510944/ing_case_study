/**
 * Calculates the history based on the PEP status.
 * @param {string} PEP_STATUS - The PEP status ('ACTIVE', 'INACTIVE', or any other value).
 * @returns {string} The calculated history value ('0' for 'ACTIVE', '1' for 'INACTIVE', '2' for other values).
 */
export function calculateHistory(PEP_STATUS) {
    if (PEP_STATUS === 'ACTIVE') {
        return '0';
    } else if (PEP_STATUS === 'INACTIVE') {
        return '1';
    } else {
        return '2';
    }
}

