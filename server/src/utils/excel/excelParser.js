import XLSX from 'xlsx';

/**
 * Parses an Excel file and returns its data as JSON.
 * @param {string} filePath - The path to the Excel file.
 * @param {Array<string>} expectedAttributes - An array containing the expected attributes in the Excel file.
 * @param {string} startRow - The starting row from which to parse the data (1-indexed).
 * @returns {Promise<Array<Object>>} A promise that resolves to an array of objects containing parsed data from the Excel file.
 */
export async function parseExcel(filePath, expectedAttributes, startRow) {
    // Load the Excel file
    const workbook = XLSX.readFile(filePath);

    // getting the first sheet of the Excel file.
    const sheetName = workbook.SheetNames[0];
    const sheet = workbook.Sheets[sheetName];

    // Convert the sheet data to JSON
    const jsonData = XLSX.utils.sheet_to_json(sheet, {
        header: expectedAttributes.map(attr => attr.replace(/ /g, '_')),
        range: startRow, // Skip rows before this number (1-indexed)
        blankrows: true // Include blank rows
    });

    // Ensure all attributes are included, even if their data is empty
    jsonData.forEach(row => {
        expectedAttributes.forEach(attr => {
            if (!(attr in row)) {
                row[attr] = ''; // Add the missing attribute with empty data
            }
        });
    });

    return jsonData;
}



