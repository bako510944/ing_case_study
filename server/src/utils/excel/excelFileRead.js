import fs from 'fs/promises';
import path from 'path';
import { fileURLToPath } from 'url';

// Get the current file path
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/**
 * Retrieves the path of the Excel file from the specified directory.
 * @returns {Promise<string|null>} A promise that resolves to the path of the Excel file if found, otherwise null.
 */
export async function getExcelFilePath() {
    // Specifying the directory path here where the file is located
    const directoryPath = path.join(__dirname, '../../files/Input files');

    try {
        // Get the list of files in the directory
        const files = await fs.readdir(directoryPath);

        // Find the first file with the .xlsx extension
        const excelFile = files.find(file => path.extname(file) === '.xlsx');

        if (excelFile) {
            // Return the path to the Excel file when founded
            return path.join(directoryPath, excelFile);
        } else {
            console.log('No Excel file found in the directory');
            return null;
        }
    } catch (error) {
        console.error('Error while reading directory:', error);
        throw error;
    }
}

/**
 * Deletes the Excel file from the specified directory.
 */
export async function deleteExcelFile() {
    try {
        // Get the path of the Excel file
        const excelFilePath = await getExcelFilePath();

        if (!excelFilePath) {
            console.log('No Excel file found to be deleted.');
            return;
        }
        // Delete the file
        await fs.unlink(excelFilePath);

        console.log('Excel file deleted successfully');
    } catch (error) {
        console.error('Error while deleting Excel file:', error);
        throw error;
    }
}

/**
 * An array containing the expected attributes in the Excel file.
 * @type {Array<string>}
 */
export const expectedAttributes = [
    'RECORD_ID',
    'SYSTEM_ID',
    'BATCH_ID',
    'STATE_NAME',
    'Name',
    'Date Of Birth',
    'Country',
    'UNIT',
    'Type of Record',
    'Hit ID',
    'Filtering Date',
    'Decision Date',
    'Alerts',
    'ORIGIN',
    'DESIGNATION',
    'KEYWORD',
    'User 1',
    'User 2',
    'USERDATA2'
];

