import path from "path";
import fs from "fs";
import {mergeRecordsWithTrueHits} from "../db/dbTableMerge.js";

/**
 * Generates a report from merged data and writes it to a text file.
 * @param {Object} db - The SQLite database object.
 */
export async function generateReportAndWriteToFile(db) {
    try {
        // Get the merged tables
        const mergedData = await mergeRecordsWithTrueHits(db);

        // Define the filename and directory path for the report file
        const filename = 'report.txt';
        const directoryPath = '../server/src/files';
        const filePath = path.join(directoryPath, filename);

        // Generate the required data for the report
        const reportContent = mergedData.map(row => `${row.ID},${row.VALUE},${row.UNIT},${row.Hit_ID}`).join('\n');

        // Write the report content to the file
        fs.writeFileSync(filePath, reportContent, (err) => {
            if (err) {
                console.error('An error occurred while writing the report to file:', err);
            } else {
                console.log('Report generated and written to file successfully.');
            }
        });
    } catch (error) {
        console.error('An error occurred:', error);
    }
}
