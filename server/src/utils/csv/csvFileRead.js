import {promises as fsPromises} from 'fs';
import path from 'path';

const directoryPath = '../server/src/files/Initialization files';

/**
 * Retrieves the list of CSV files from the specified directory.
 * @returns {Promise<Array<string>>} A promise that resolves to an array of file paths to CSV files.
 */
export async function getCsvFiles() {
    try {
        const filenames = await fsPromises.readdir(directoryPath);
        // Filter out filenames that do not end with '.csv', map them to their full paths, and sort them by date of month
        return filenames
            .filter(filename => filename.endsWith('.csv'))
            .map(filename => path.join(directoryPath, filename))
            .sort(sortByDate);
    } catch (error) {
        console.error('Error reading directory:', error);
        return [];
    }
}

/**
 * Deletes all CSV files from the specified directory.
 */
export async function deleteCsvFiles() {
    try {
        const filenames = await fsPromises.readdir(directoryPath);
        for (const filename of filenames) {
            if (filename.endsWith('.csv')) {
                const filePath = path.join(directoryPath, filename);
                await fsPromises.unlink(filePath);
                console.log(`Deleted file: ${filePath}`);
            }
        }
        console.log('All CSV files deleted successfully.');
    } catch (error) {
        console.error('Error deleting CSV files:', error);
    }
}

/**
 * Extracts the date from a CSV filename.
 * @param {string} filename - The filename from which to extract the date.
 * @returns {string} The extracted date from the filename.
 */
function getDateFromFilename(filename) {
    const match = filename.match(/(\d{8})\.csv$/);
    return match ? match[1] : '';
}

/**
 * Comparator function to sort filenames by date.
 * @param {string} a - The first filename to compare.
 * @param {string} b - The second filename to compare.
 * @returns {number} A negative value if 'a' comes before 'b', a positive value if 'a' comes after 'b', or zero if they are equal.
 */
function sortByDate(a, b) {
    const dateA = getDateFromFilename(a);
    const dateB = getDateFromFilename(b);
    return dateA.localeCompare(dateB);
}

