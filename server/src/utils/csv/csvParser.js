import csv from 'csv-parser';
import fs from 'fs';
/**
 * Parses multiple CSV files and returns their data.
 * @param {Array<string>} csvFiles - An array of file paths to CSV files.
 * @returns {Promise<Array<Object>>} A promise that resolves to an array of objects containing parsed data from CSV files.
 */
export async function parseCSVFiles(csvFiles) {
    try {
        let dataFromCSVFiles = [];

        for (const filename of csvFiles) {
            const parsedData = await parseCSV(filename);
            dataFromCSVFiles.push(...parsedData);
            console.log(`CSV file ${filename} successfully read.`);
        }

        return dataFromCSVFiles;
    } catch (error) {
        console.error('Error parsing CSV files:', error);
        return [];
    }
}
/**
 * Parses a single CSV file and returns its data.
 * @param {string} filename - The file path to the CSV file.
 * @returns {Promise<Array<Object>>} A promise that resolves to an array of objects containing parsed data from the CSV file.
 */
async function parseCSV(filename) {
    const stream = fs.createReadStream(filename);
    const parsedData = [];

    try {
        // Iterate through each row in the CSV file
        for await (const row of stream.pipe(csv())) {
            // Extract required fields from each row and construct a parsed row object
            const { ID, VALUE, PEP_STATUS } = row;
            const parsedRow = {
                ID: ID || '',
                VALUE: VALUE || '',
                PEP_STATUS: PEP_STATUS || ''
            };

            parsedData.push(parsedRow);
        }
        console.log(`CSV file ${filename} successfully parsed.`);
    } catch (error) {
        console.error('Error parsing CSV file:', error);
    }

    return parsedData;
}
