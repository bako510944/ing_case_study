import express from 'express';
import {getUsers, getUser, addNewUser, updateUserInfo, deleteUser} from "../controllers/users.js";

const router = express.Router();

router.get('/',getUsers);
router.get('/:id', getUser);
router.post('/',addNewUser);
router.put('/:id',updateUserInfo);
router.delete('/:id',deleteUser);

export default router;