import express from 'express';
import { uploadCSV } from '../controllers/records.js';
import { getCSVFilename } from "../middlewares/get_original_filename.js";

const router = express.Router();

router.post('/', getCSVFilename.single('csvFile'), uploadCSV);
export default router;
