import express from 'express';
import { uploadExcel } from '../controllers/trueHits.js';
import { getExcelFilename } from "../middlewares/get_original_filename.js";

const router = express.Router();

router.post('/', getExcelFilename.single('excelFile'), uploadExcel);
export default router;

