import express from 'express';
import { reportDistribution } from '../controllers/distributions.js';

const router = express.Router();

router.post('/',reportDistribution);

export default router;