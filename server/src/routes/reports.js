import express from 'express';
import { getReportFile } from '../controllers/reports.js';

const router = express.Router();

router.get('/', getReportFile);

export default router;
