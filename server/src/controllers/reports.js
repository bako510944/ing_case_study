import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

export function getReportFile(req, res) {
    try {
        const __filename = fileURLToPath(import.meta.url);
        const dirname = path.dirname(__filename);
        const filePath = path.join(dirname, '../files/report.txt');
        const fileContent = fs.readFileSync(filePath, 'utf-8');
        console.log(fileContent);
        res.send(fileContent);
    } catch (error) {
        console.error('An error occurred while reading the report file:', error);
        res.status(500).send('Internal server error');
    }
}

