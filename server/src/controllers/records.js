import { parseCSVFiles } from '../utils/csv/csvParser.js';

export async function uploadCSV(req, res) {
    try {
        const csvFile = req.file;
        const parsedData = await parseCSVFiles([csvFile.path]);
        parsedData.forEach(row => {
            console.log(row);
        });
        res.send('CSV file uploaded and processed successfully');
    } catch (error) {
        console.error('Error uploading and processing CSV file:', error);
        res.status(500).send('Internal server error');
    }
}
