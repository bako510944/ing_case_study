import {getAllUsers, getUserById, addUser, updateUser, deleteUserById} from '../models/users.js';
import HttpStatus from 'http-status-codes';

export async function getUsers(req, res) {
    try {
        const users = await getAllUsers();
        res.status(HttpStatus.OK).json(users);
    } catch (error) {
        console.error('Error fetching users:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal server error' });
    }
}
export async function getUser(req, res) {
    try {
        const { id } = req.params;
        console.log('id ',id);
        const user = await getUserById(id);

        res.status(HttpStatus.OK).json(user);
    } catch (error) {
        console.error('Error fetching user:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal server error' });
    }
}
export async function addNewUser(req, res) {
    try {
        const { email } = req.body;

        await addUser(email);

        res.status(HttpStatus.CREATED).json({
            msg: 'User added successfully'
        });
    } catch (error) {
        console.error('Error adding user:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal server error' });
    }
}

export async function updateUserInfo(req, res) {
    try {
        const { id } = req.params;
        const { email } = req.body;

        await updateUser(id, email);

        res.status(HttpStatus.OK).json({
            msg: 'User updated successfully'
        });
    } catch (error) {
        console.error('Error updating user:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal server error' });
    }
}

export async function deleteUser(req, res) {
    try {
        const { id } = req.params;

        await deleteUserById(id);
        res.status(HttpStatus.OK).json({
            msg: 'User deleted successfully'
        });
    } catch (error) {
        console.error('Error deleting user:', error);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal server error' });
    }
}