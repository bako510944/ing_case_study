import nodemailer from 'nodemailer';
import {getAllUsers} from "../models/users.js";

export async function reportDistribution(req, res){
    try {
        const users = await getAllUsers();
        const reportPath = '../server/src/files/report.txt';

        for (const user of users) {
            const email = user.email;
            await sendEmailWithReport(email, reportPath);
        }

        res.status(200).send('Report sent to all users successfully.');
    } catch (error) {
        console.error('Error sending report to users:', error);
        res.status(500).send('Internal server error');
    }
}
async function sendEmailWithReport(email, reportPath) {
    // Create Nodemailer transporter
    const transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'amirdoorwerth@gmail.com',
            pass: process.env.PASSWORD
        }
    });

    // Email options
    const mailOptions = {
        from: 'amirdoorwerth@gmail.com',
        to: email,
        subject: 'Report',
        text: 'Please find attached the report.',
        attachments: [
            {
                path: `${reportPath}`
            }
        ]
    };

    // Send email
    await transporter.sendMail(mailOptions, (err) => {
        if (err) {
            console.log('Error: ', err);
        } else {
            console.log("Report has been sent to the users.");
        }
    });
}