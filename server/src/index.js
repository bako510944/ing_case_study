import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import recordRouter from './routes/records.js';
import trueHitRouter from './routes/trueHits.js';
import reportRouter from './routes/reports.js';
import userRouter from './routes/users.js';
import distributionRouter from './routes/distributions.js';
import { dbExecution } from './db/dbExecuter.js';

const app = express();
const PORT = 3000;
dotenv.config();

const execute = dbExecution();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes
app.use('/records', recordRouter);
app.use('/trueHits', trueHitRouter);
app.use('/reports', reportRouter);
app.use('/users', userRouter);
app.use('/distributions',distributionRouter);

// Start the server
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

